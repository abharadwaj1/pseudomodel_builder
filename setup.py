import setuptools
from setuptools import setup
#from numpy.distutils.core import setup, Extension


setup(name='pseudomodel',
    version='0.1',
    author='Alok Bharadwaj',
    url='https://gitlab.tudelft.nl/abharadwaj1/pseudomodel_builder',
    description= 'Build a pseudo-atomic model for a given cryo EM density map',
    license='3-clause BSD',
    packages=setuptools.find_packages(),
    install_requires=['numpy>=1.19.5','scipy>=1.5.4','mrcfile>=1.3.0','gemmi>=0.4.8','sklearn>=0.0','pwlf>=2.0.4','tqdm>=4.62.3','more_itertools>=8.10.0'],
    zip_safe= False)

